﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;  //!!!!!
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;

namespace Calc
{
    class ViewModel : INotifyPropertyChanged
    {

        public ViewModel()
        {
            _buttonCommand = new RelayCommand<string>(x => EXP += x);
        }

        private string _EXP;
        public string EXP { get { return _EXP; } set { _EXP = value; OnPropertyChanged(nameof(EXP)); } }

		public List<string> Memory = new List<string>();

        private ICommand _eqCommand = new EqCommand();
        private ICommand _bsCommand = new BSCommand();
        private ICommand _resCommand = new RESCommand();
        private ICommand _buttonCommand;
		private ICommand _addmemCommand = new AddMemCommand();
		private ICommand _ejectmemCommand = new EjectMemCommand();
		private ICommand _clearmemCommand = new ClearMemCommand();
		public ICommand EqCommand { get { return _eqCommand; } }
        public ICommand BSCommand { get { return _bsCommand; } }
        public ICommand RESCommand { get { return _resCommand; } }
        public ICommand ButtonCommand { get { return _buttonCommand; } }
        public ICommand AddMemCommand { get { return _addmemCommand; } }
        public ICommand EjectMemCommand { get { return _ejectmemCommand; } }
        public ICommand ClearMemCommand { get { return _clearmemCommand; } }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class ButtonCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            //var viewModel = parameter as ViewModel;

            //if (viewModel == null)
            //    return false;
            return true;
        }

        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;
            viewModel.EXP += parameter.ToString();
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class EqCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            if (viewModel == null)
                return false;

            Parser P = new Parser(viewModel.EXP);
            return string.IsNullOrEmpty(viewModel.EXP) == false && P.IsError() == false;
        }

        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            Parser P = new Parser(viewModel.EXP);
            double result = P.RUN();
            if (P.code_errors == result)
                viewModel.EXP = "ERROR";
            else
                viewModel.EXP = result.ToString();
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class BSCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            if (viewModel == null)
                return false;

            return string.IsNullOrEmpty(viewModel.EXP) == false;
            //&& double.TryParse(viewModel.EXP, out parseResult); reg выражения
        }

        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            viewModel.EXP = viewModel.EXP.Substring(0, viewModel.EXP.Length - 1);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RESCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            if (viewModel == null)
                return false;

            return string.IsNullOrEmpty(viewModel.EXP) == false;
            //&& double.TryParse(viewModel.EXP, out parseResult); reg выражения
        }

        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            viewModel.EXP = null;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

	public class AddMemCommand : ICommand
	{
		public bool CanExecute(object parameter)
		{
			var viewModel = parameter as ViewModel;

			if (viewModel == null)
				return false;

			Parser P = new Parser(viewModel.EXP);
			return string.IsNullOrEmpty(viewModel.EXP) == false && P.IsError() == false;
		}

		public void Execute(object parameter)
		{
			var viewModel = parameter as ViewModel;
			Parser P = new Parser(viewModel.EXP);
			viewModel.Memory.Add(Convert.ToString(P.RUN()));
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
			}
		}
	}

	public class EjectMemCommand : ICommand
	{
		public bool CanExecute(object parameter)
		{
			var viewModel = parameter as ViewModel;
			if (viewModel == null)
				return false;
			return viewModel.Memory.Any();
		}

		public void Execute(object parameter)
		{
			var viewModel = parameter as ViewModel;
			viewModel.EXP += viewModel.Memory.Last();
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
			}
		}
	}

	public class ClearMemCommand : ICommand
	{
		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			var viewModel = parameter as ViewModel;
			viewModel.Memory.Clear();
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
			}
		}
	}


	public class Parser
    {
        private char[] EXPRESSION;
        private int i;
        private int long_exp;
        private char buf;
        public double code_errors;

        public Parser(string expression = "")
        {
            if (expression == null)
                expression = "";
            EXPRESSION = expression.ToCharArray();
            i = 0;
            code_errors = -1.00010001;
            long_exp = EXPRESSION.Length;
            buf = ';';
        }

        public bool IsError()
        {
            return RUN() == code_errors;
        }

        public void GET() //Записывает символы в буфер
        {
            while (i < long_exp)
            {
                if (EXPRESSION[i] == ' ')
                    i++;
                else
                    break;
            }

            if (i < long_exp)
            {
                buf = EXPRESSION[i++];
            }
            else
            {
                i = 0;
                buf = ';';
            }
        }

        public char GetBuf() => buf;

        public double RUN()
        {
            GET();
            double x = PRO_E();
            if (buf == ')' || buf == '(')
                return code_errors;
            else
                return x;
        }

        private double PRO_E()
        {
            double x = PRO_T();
            while ((buf == '+' || buf == '-')&&x!=code_errors)
            {
                char p = buf;
                double tx;
                GET();
                if (p == '+')
                {
                    tx = PRO_E();
                    if (tx == code_errors)
                        return tx;
                    else
                        x += tx;
                }
                else
                {
                    tx = PRO_E();
                    if (tx == code_errors)
                        return tx;
                    else
                        x -= tx;
                }
            }
            return x;
        }

        private double PRO_T()
        {
            double x = PRO_M();
            double tx;
            while ((buf == '*' || buf == '/' || buf == '×' || buf == '÷' || buf == '^') && x != code_errors)
            {
                char p = buf;
                GET();
                if (p == '*' || p == '×')
                {
                    tx = PRO_E();
                    if (tx == code_errors)
                        return tx;
                    else
                        x *= tx;
                }
                else
                {
					if (p == '/' || p == '÷')
					{
						tx = PRO_E();
						if (tx == code_errors)
							return tx;
						else
							x /= tx;
					}
					else
					{
						tx = PRO_E();
						if (tx == code_errors)
							return tx;
						else
							x = Math.Pow(x, tx);
					}
                }
            }
            return x;
        }

        private double PRO_M()
        {
            double x;

            if (buf == '(')                    // выражение в скобках
            {
                GET();
                x = PRO_E();
                if (buf != ')')                     // скобку закрыть не забыли?
                {
                    return code_errors;
                }
                GET();
            }
            else
            {
                double signe = 1;

                if (buf == '-')
                {
                    signe = -1;
                    GET();
                }


                if (char.IsDigit(buf))    // константа
                    x = signe * PRO_C();
                else
                    return code_errors;
            }
            return x;            // возвращаем найденное значение
        }

        internal double PRO_C() //private
        {
            string number = string.Empty;
            int i = 0;
            while (char.IsDigit(buf) || buf == '.' || buf == ',')    // пока идут цифры
            {
                if (buf == '.' || buf == ',') 
                { 
                    if (i>0) { return code_errors; }
                    i++; 
                }
                if (buf == '.') { buf = ','; }
                number += buf;
                GET();
            }
            return double.Parse(number);
        }
    }

}
