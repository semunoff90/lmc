﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoselCalc
{
    public class Parser
    {
        private
            char[] EXPRESSION;
        int i;
        int long_exp;
        char buf;
        double code_errors;

        public Parser(string expression = "")
        {
            EXPRESSION = expression.ToCharArray();
            i = 0;
            code_errors = -1.00010001;
            long_exp = EXPRESSION.Length;
            buf = ';';
        }

        public void GET() //Записывает символы в буфер
        {
            while (i < long_exp)
            {
                if (EXPRESSION[i] == ' ')
                    i++;
                else
                    break;
            }

            if (i < long_exp)
            {
                buf = EXPRESSION[i++];
            }
            else
            {
                i = 0;
                buf = ';';
            }
        }

        public char GetBuf() => buf;

        public double RUN()
        {
            GET();
            double x = PRO_E();
            if (buf == ')')
                return code_errors;
            else
                return x;
        }

        private double PRO_E()
        {
            double x = PRO_T();
            while (buf == '+' || buf == '-')
            {
                char p = buf;
                double tx;
                GET();
                if (p == '+')
                {
                    tx = PRO_M();
                    if (tx == code_errors)
                        x = tx;
                    else
                        x += tx;
                }
                else
                {
                    tx = PRO_M();
                    if (tx == code_errors)
                        x = tx;
                    else
                        x -= tx;
                }
            }
            return x;
        }

        private double PRO_T()
        {
            double x = PRO_M();
            double tx;
            while (buf == '*' || buf == '/')
            {
                char p = buf;
                GET();
                if (p == '*')
                {
                    tx = PRO_M();
                    if (tx == code_errors)
                        x = tx;
                    else
                        x *= tx;
                }
                else
                {
                    tx = PRO_M();
                    if (tx == code_errors)
                        x = tx;
                    else
                        x /= tx;
                }
            }
            return x;
        }

        private double PRO_M()
        {
            double x;

            if (buf == '(')                    // выражение в скобках
            {
                GET();
                x = PRO_E();
                if (buf != ')')                     // скобку закрыть не забыли?
                {
                    return code_errors;
                }
                GET();
            }
            else
            {
                double signe = 1;

                if (buf == '-')
                {
                    signe = -1;
                    GET();
                }

                if (buf == '+')
                {
                    signe = +1;
                    GET();
                }


                if (char.IsDigit(buf))    // константа
                    x = signe * PRO_C();
                else
                    return code_errors;
            }
            return x;            // возвращаем найденное значение
        }

        internal double PRO_C() //private
        {
            string number = string.Empty;
            while (char.IsDigit(buf) || buf == '.' || buf == ',')    // пока идут цифры
            {
                if (buf == '.') { buf = ','; }
                number += buf;
                GET();
            }
            return double.Parse(number);
        }
    }
}
